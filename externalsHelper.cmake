include(ExternalProject)

function(AddAndInstallFromGit)
    cmake_parse_arguments(
        IN # prefix of output variables
        "" # list of names of the boolean arguments (only defined ones will be true)
        "PROJ_NAME;GIT_LINK;HEADERS_PATH;LIBS_PATH" # list of names of mono-valued arguments
        "" # list of names of multi-valued arguments (output variables are lists)
        ${ARGN} # arguments of the function to parse, here we take the all original ones
    )
    set(BUILD_PATH_OF_THE_EXTERNAL "${CMAKE_CURRENT_LIST_DIR}/build/${IN_PROJ_NAME}/src/${IN_PROJ_NAME}External")

    ExternalProject_Add("${IN_PROJ_NAME}External"
        GIT_REPOSITORY ${IN_GIT_LINK}
        PREFIX "${CMAKE_CURRENT_LIST_DIR}/build/${IN_PROJ_NAME}"
    )
    
    add_custom_target("${IN_PROJ_NAME}InstallDirs"
        COMMAND mkdir -p ${EXTERNALS_INCLUDE_DIR}/${IN_PROJ_NAME} || true 
        COMMAND mkdir -p ${EXTERNALS_LIB_DIR} || true
        COMMENT "Creating ${IN_PROJ_NAME} install locations"
    )
    add_custom_target("${IN_PROJ_NAME}HeadersInstallation"
        COMMAND cp -r ${IN_HEADERS_PATH}/* ${EXTERNALS_INCLUDE_DIR}/${IN_PROJ_NAME}
        COMMENT "Installing ${IN_PROJ_NAME} headers"
    )
    add_custom_target("${IN_PROJ_NAME}LibsInstallation"
        COMMAND cp -r ${IN_LIBS_PATH}/* ${EXTERNALS_LIB_DIR}
        COMMENT "Installing ${IN_PROJ_NAME} libs"
    )

    add_dependencies(${TARGET} "${IN_PROJ_NAME}HeadersInstallation" "${IN_PROJ_NAME}LibsInstallation")
    add_dependencies("${IN_PROJ_NAME}HeadersInstallation" "${IN_PROJ_NAME}External")
    add_dependencies("${IN_PROJ_NAME}LibsInstallation" "${IN_PROJ_NAME}External")
    add_dependencies("${IN_PROJ_NAME}External" "${IN_PROJ_NAME}InstallDirs")
endfunction()


function(CustomAddAndInstallFromGit)
    cmake_parse_arguments(
        IN # prefix of output variables
        "" # list of names of the boolean arguments (only defined ones will be true)
        "PROJ_NAME;GIT_LINK;HEADERS_PATH;LIBS_PATH;CONFIGURE_CWD;BUILD_CWD" # list of names of mono-valued arguments
        "CONFIGURE_COMMAND;BUILD_COMMAND" # list of names of multi-valued arguments (output variables are lists)
        ${ARGN} # arguments of the function to parse, here we take the all original ones
    )
    set(BUILD_PATH_OF_THE_EXTERNAL "${CMAKE_CURRENT_LIST_DIR}/build/${IN_PROJ_NAME}/src/${IN_PROJ_NAME}External")

    if("" STREQUAL "${IN_BUILD_COMMAND}")
        set(BUILD_CWD "${CMAKE_CURRENT_LIST_DIR}")
    endif()

    if("" STREQUAL "${IN_CONFIGURE_COMMAND}")
        set(CONFIGURE_CWD "${CMAKE_CURRENT_LIST_DIR}")
    endif()



    ExternalProject_Add("${IN_PROJ_NAME}External"
        GIT_REPOSITORY ${IN_GIT_LINK}
        PREFIX "${CMAKE_CURRENT_LIST_DIR}/build/${IN_PROJ_NAME}"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
    )
    

    add_custom_target("${IN_PROJ_NAME}Configure"
        COMMAND ${IN_CONFIGURE_COMMAND}
        WORKING_DIRECTORY "${IN_CONFIGURE_CWD}"
        COMMENT "Configuring ${IN_PROJ_NAME}..."
    )

    add_custom_target("${IN_PROJ_NAME}Build"
        COMMAND ${IN_BUILD_COMMAND}
        WORKING_DIRECTORY "${IN_BUILD_CWD}"
        COMMENT "Building ${IN_PROJ_NAME}"
    )

    add_custom_target("${IN_PROJ_NAME}InstallDirs"
        COMMAND mkdir -p ${EXTERNALS_INCLUDE_DIR}/${IN_PROJ_NAME} || true
        COMMAND mkdir -p ${EXTERNALS_LIB_DIR} || true
        COMMENT "Creating ${IN_PROJ_NAME} install locations"
    )


    if(NOT "" STREQUAL "${IN_HEADERS_PATH}")
        add_custom_target("${IN_PROJ_NAME}HeadersInstallation"
            COMMAND cp ${IN_HEADERS_PATH}/*.h* ${EXTERNALS_INCLUDE_DIR}/${IN_PROJ_NAME}
            COMMENT "Installing ${IN_PROJ_NAME} headers"
        )
    endif()

    if(NOT "" STREQUAL "${IN_LIBS_PATH}")
        add_custom_target("${IN_PROJ_NAME}LibsInstallation"
            COMMAND find ${IN_LIBS_PATH}/ -type f -name *.a -exec cp {} ${EXTERNALS_LIB_DIR} "\;"
            COMMAND find ${IN_LIBS_PATH}/ -type f -name *.so -exec cp {} ${EXTERNALS_LIB_DIR} "\;"
            COMMENT "Installing ${IN_PROJ_NAME} libs"
        )
    endif()



    if(NOT "" STREQUAL "${IN_HEADERS_PATH}")
        add_dependencies(${TARGET} "${IN_PROJ_NAME}HeadersInstallation" )
            add_dependencies("${IN_PROJ_NAME}HeadersInstallation" "${IN_PROJ_NAME}External")
            add_dependencies("${IN_PROJ_NAME}HeadersInstallation" "${IN_PROJ_NAME}InstallDirs")
    endif()

    if(NOT "" STREQUAL "${IN_LIBS_PATH}")
        add_dependencies(${TARGET} "${IN_PROJ_NAME}LibsInstallation")
            add_dependencies("${IN_PROJ_NAME}LibsInstallation" "${IN_PROJ_NAME}InstallDirs")
            add_dependencies("${IN_PROJ_NAME}LibsInstallation" "${IN_PROJ_NAME}Build")
                add_dependencies("${IN_PROJ_NAME}Build" "${IN_PROJ_NAME}Configure")
                    add_dependencies("${IN_PROJ_NAME}Configure" "${IN_PROJ_NAME}External")
    else()
        add_dependencies(${TARGET} "${IN_PROJ_NAME}Build")
            add_dependencies("${IN_PROJ_NAME}Build" "${IN_PROJ_NAME}Configure")
                add_dependencies("${IN_PROJ_NAME}Configure" "${IN_PROJ_NAME}External")
                    add_dependencies("${IN_PROJ_NAME}External" "${IN_PROJ_NAME}InstallDirs")
    endif()
    
endfunction()

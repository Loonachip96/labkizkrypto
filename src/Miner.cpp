#include "Miner.h"
#include "AtomicPrint.h"
#include <CryptoCpp/sha.h>
#include <vector>

Miner::Miner() = default;
Miner::~Miner() = default;

auto Miner::feed(
    Digest previousBlockHash,
    BigNumber previousBlockID,
    std::list<std::string> newTransactions, 
    size_t PoWZerosAmount
) -> void {
    this->isMining = true;
    this->PoWzerosCount = PoWZerosAmount;
    this->transactions = newTransactions;

    this->blockBeingMined.prevBlockDigest = previousBlockHash.asBigNumber();
    this->blockBeingMined.prevBlockID = previousBlockID;
    this->blockBeingMined.timestamp = time(0);
    this->blockBeingMined.rootMerkleDigest = this->getMerkleDigest();
}

auto Miner::abandonWork() -> void{
    this->shouldAbandonWork = true;
    this->isMining = false;
}

auto Miner::isBusy() -> bool{
    return this->isMining;
}

auto Miner::nBitOf(size_t nBit, Digest& digest) -> int{
    int nByte = std::floor( (double)nBit/8 );
    CryptoPP::byte byteToLookForBit = digest[nByte];
    CryptoPP::byte byteShifted = byteToLookForBit >> (nBit % 8);
    return (byteShifted & 0x1);
}

auto Miner::getMerkleDigest() -> BigNumber{
    
    // getting first Merkle tree hashes level
    std::vector<Digest> transactionsHashes;
    for (auto &&t : this->transactions){
        Digest d;
        d.calculateDigestFor(t);
        transactionsHashes.push_back(d);
    }
    
    // Merkle tree hashes aggregation
    size_t treeDepth = log2(this->transactions.size());
    for (size_t treeLevel = 0; treeLevel < treeDepth; treeLevel++){
        std::vector<Digest> nextLevelHashes;

        for (size_t tHash = 0; tHash < transactionsHashes.size(); tHash += 2){
            std::string concatenatedHashesPair = 
                transactionsHashes.at(tHash).asStdString()
                + transactionsHashes.at(tHash+1).asStdString();

            Digest d;
            d.calculateDigestFor(concatenatedHashesPair);
            nextLevelHashes.push_back( d );
        }
        transactionsHashes = std::move(nextLevelHashes);
    }
    
    return transactionsHashes.at(0).asBigNumber();
}


auto Miner::areNLeftBitsZeros(
    size_t n, 
    Digest& digest
) -> bool {
    static const size_t bitsInBytes = 8;
    static const int digestSize = Digest::DIGEST_SIZE * bitsInBytes;

    for (size_t i = digestSize-1; i >= digestSize-n; i--){
        int b = this->nBitOf(i, digest);
        if( b != 0 ){
            return false;
        }
    }
    return true;
}

auto Miner::mine() -> MineralsFound{
    this->isMining = true;
    MineralsFound result{{}, ""};

    auto zerosAmount = this->PoWzerosCount;
    Digest blockHeaderDigest;

    this->blockBeingMined.randomNumber = std::uniform_int_distribution<int>{}(this->randomEngine);
    do{
        this->blockBeingMined.randomNumber += 1;
        blockHeaderDigest.assignDigest( this->blockBeingMined.getDigest() );
    }while(
        (! this->shouldAbandonWork)
        && (! this->areNLeftBitsZeros(zerosAmount, blockHeaderDigest)) 
    );
    
    if(! this->shouldAbandonWork)
        result = MineralsFound(
            this->blockBeingMined, 
            blockHeaderDigest.asBigNumber()
        );

    this->shouldAbandonWork = false;
    this->isMining = false;
    return result;
}

auto Miner::bitDump(BigNumber number) -> void{
    auto numberStr = number.getString();
    Atomic::cout output;

    for (size_t c = 0; c < numberStr.size(); c++)
    {
        for (int i = 7; i >= 0; i--){
            int bit = (numberStr.at(c) >> i) & 0x1;
            output << bit;
        }
        output << " ";

        if(c % 4 == 0)
            output << Atomic::cout::endl;
    }


    output << Atomic::cout::endl << Atomic::cout::flush;
}
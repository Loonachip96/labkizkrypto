#include "Timer.h"
#include "Nodes/Mine.h"
#include "StaticVars.h"
#include "AtomicPrint.h"

namespace {
    using Node_t = MineNodeFactory::Node_t;
    using NodeData_t = MineNodeFactory::NodeData_t;
    using NodeInstance_t = MineNodeFactory::NodeInstance_t;

    constexpr static const bool ACTIVATE_ON_EDGE_CREATION = true;
    constexpr static const bool ACTIVATE_ON_DEMAND = false;
    constexpr static const bool CONTINUE_GENERATION = true;
    constexpr static const bool END_GENERATION = false;

    static unsigned int randomIterator = 0;
    static const std::array<std::string, 6> actors{ "A", "B", "C", "D", "E", "F" };
    static const std::array<std::string, 7> amounts{ "5", "20", "100", "7", "15", "3", "42" };
}

auto MineNodeFunctional::operator()(
    TaggedDebris& taggedDebris
) -> bool {
    if (Static::doExit || this->generatedTransactionsCount >= Static::NUM_TRANSACTIONS) 
        return END_GENERATION;

    PeriodWaiterTimer{std::chrono::milliseconds{1000}};

    auto & t = std::get<0>(taggedDebris); // t stands for transaction
        t = "from:";    t += actors[randomIterator%actors.size()]; t += " ";
        t += "to:";     t += actors[(randomIterator+1)%actors.size()]; t += " ";
        t += "amount:"; t += amounts[(3*randomIterator)%amounts.size()];
        randomIterator++;

    Atomic::cout{} << "[TheMine]\t\t\tnew transaction generated: " << t << Atomic::cout::endlAndFlush;

    Static::pushTransaction(t);

    auto & tag = std::get<1>(taggedDebris);
        tag.demandedPoWLevel = Static::PoW_LEVEL;
        tag.headBlockDigest = Static::getHeadDigest();
        tag.headBlockID = Static::getHeadID();

    generatedTransactionsCount++;
    return CONTINUE_GENERATION;
}

NodeInstance_t MineNodeFactory::Create(int objectID){
    NodeInstance_t n;

    n.data = std::make_shared<NodeData_t>();
    n.node = std::make_shared<Node_t>(
        *_graphPtr, 
        MineNodeFunctional{objectID}, 
        ACTIVATE_ON_DEMAND
    );

    return n;
}
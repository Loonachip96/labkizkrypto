#include "Nodes/Silversmith.h"
#include "Digest.h"
#include "BlockHeader.h"
#include "AtomicPrint.h"
#include "StaticVars.h"

namespace{
    using Node_t = SilversmithFactory::Node_t;
    using NodeData_t = SilversmithFactory::NodeData_t;
    using NodeInstance_t = SilversmithFactory::NodeInstance_t;
}

std::mutex SilversmithFunctional::m;

auto SilversmithFunctional::operator()(
    const IndexedMineralPack& indexedMinerals
) -> void{
    m.lock();

    auto minerals = tbb::flow::cast_to<MineralsFound>(indexedMinerals);

    if(
        minerals.first.prevBlockDigest.getString() == Static::getHeadDigest().getString()
        && minerals.first.prevBlockID == Static::getHeadID()
    ){
        static const char DELIMITER = ' ';
        std::string serialisedBlock;

        // attach transactions
        int startTransactionIndex = (Static::getChainLength()-1) * Miner::TRANSACTIONS_PER_TREE;
        for (size_t t = 0; t < Miner::TRANSACTIONS_PER_TREE; t++){
            serialisedBlock += Static::getTransaction(startTransactionIndex+t) + DELIMITER;
        }
        // attach block header data to new block
        serialisedBlock += minerals.first.prevBlockDigest.getString() + DELIMITER;
        serialisedBlock += minerals.first.prevBlockID.getString() + DELIMITER;
        serialisedBlock += minerals.first.randomNumber.getString() + DELIMITER;
        serialisedBlock += minerals.first.rootMerkleDigest.getString() + DELIMITER;
        serialisedBlock += std::to_string(minerals.first.timestamp) + DELIMITER;

        // attach block header digest to it
        serialisedBlock += minerals.first.getDigest().getString();


        // calculate new block digest 
        Digest blockDigest;
        blockDigest.calculateDigestFor(serialisedBlock);

        Static::pushNewBlock( serialisedBlock, blockDigest.asBigNumber() );
        Atomic::cout{}  << "[Silversmith]\t\t\tMiner <" << static_cast<int>(indexedMinerals.tag()) << "> MANAGED TO ADD NEW BLOCK!!!" << Atomic::cout::endlAndFlush
                        << "[Silversmith]\tblock:  " << serialisedBlock << Atomic::cout::endlAndFlush;
    }

    m.unlock();
}

NodeInstance_t SilversmithFactory::Create(int objectID){
    NodeInstance_t n;

    n.data = std::make_shared<NodeData_t>();
    n.node = std::make_shared<Node_t>( 
        *_graphPtr, 
        tbb::flow::unlimited,
        SilversmithFunctional{objectID}
    );

    return n;
}


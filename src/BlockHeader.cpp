#include "BlockHeader.h"
#include "Digest.h"
#include <cstring>
#include <cassert>
#define NDEBUG

BlockHeader::BlockHeader() = default;
BlockHeader::~BlockHeader() = default;

auto BlockHeader::getDigest() -> BigNumber{
    auto dataBlob = this->serialise().toStdString();
    Digest digest;
    digest.calculateDigestFor(dataBlob);
    assert(digest.size() == Digest::DIGEST_SIZE);
    return digest.asBigNumber();
}

auto BlockHeader::serialise()-> DataBlob{
    const int serialisedFieldsAmount = 5;
    
    std::array<std::string, serialisedFieldsAmount> valueStrings{
        this->rootMerkleDigest.getString(),
        this->prevBlockDigest.getString(),
        std::to_string(this->timestamp),
        this->prevBlockID.getString(),
        this->randomNumber.getString()
    };

    std::string serialisedString = "";
    for (auto &&s : valueStrings){
        serialisedString += s;
    }

    auto dataBlob = DataBlob{serialisedString.c_str()};

    return dataBlob;
}
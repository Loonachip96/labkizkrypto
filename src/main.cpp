#include "Nodes/Mine.h"
#include "Nodes/CargoConveyor.h"
#include "Nodes/DebrisLoader.h"
#include "Nodes/Miner.h"
#include "Nodes/Silversmith.h"

#include "Nodes/PairingHelpers.h"
#include <TBB/flow_graph.h>
#include "Timer.h"


int main(){
    using namespace tbb::flow;

    graph g;

    MineNode mine{g};
    CargoConveyorNodes<NUM_ACTORS> cargoConveyors{g};
    DebrisLoaderNodes<NUM_ACTORS> debrisLoaders{g};
    MinerNodes<NUM_ACTORS> miners{g};
    Haulier_t haulier{g};
    SilversmithNode silversmith{g};

    MakeEdges::oneToAll(mine, cargoConveyors);
    MakeEdges::parallel(cargoConveyors, debrisLoaders);
    MakeEdges::parallel(debrisLoaders, miners);

    make_edge(miners.getNode(0), input_port<0>(haulier));
    make_edge(miners.getNode(1), input_port<1>(haulier));
    make_edge(miners.getNode(2), input_port<2>(haulier));
    make_edge(miners.getNode(3), input_port<3>(haulier));

    make_edge(haulier, silversmith.getNode());


    mine.getNode().activate();
    g.wait_for_all();

    return 0;
}
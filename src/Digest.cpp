#include "Digest.h"
#include <CryptoCpp/sha.h>
#include <cstring>
#include <cassert>

Digest::~Digest() = default;
Digest::Digest() = default;

Digest::Digest(std::string inDigest){
    this->digest = DataBlob{inDigest.c_str()};
    //assert(this->digest.size() == DIGEST_SIZE);
}

Digest::Digest(const Digest& inDigest){
    this->digest = DataBlob{inDigest.digest};
    //assert(this->digest.size() == DIGEST_SIZE);
}

Digest::Digest(Digest&& inDigest){
    this->digest = std::move(inDigest.digest);
    //assert(this->digest.size() == DIGEST_SIZE);
}

auto Digest::operator=(BigNumber inDigest) -> Digest&{
    this->digest = DataBlob{ inDigest.getString().c_str() };
    //assert(this->digest.size() == DIGEST_SIZE);
    return *this;
}
auto Digest::operator[](int i) -> int{
    //assert(this->digest.size() == DIGEST_SIZE);
    return this->digest[i];
}

auto Digest::calculateDigestFor(std::string& message) -> void{
    this->digest.allocate(Digest::DIGEST_SIZE);

    CryptoPP::SHA256{}.CalculateDigest(
        this->digest.toCryptoBytesPtr(), 
        reinterpret_cast<const CryptoPP::byte*>(message.data()), 
        message.size()
    );
    //assert(this->digest.size() == DIGEST_SIZE);
}

auto Digest::assignDigest(CryptoPP::byte* inDigest) -> void{
    this->digest = DataBlob{inDigest, DIGEST_SIZE};
    //assert(this->digest.size() == DIGEST_SIZE);
}
auto Digest::assignDigest(BigNumber inDigest) -> void{
    this->digest = DataBlob{inDigest.getString().c_str()};
    //assert(this->digest.size() == DIGEST_SIZE);
}
auto Digest::assignDigest(std::string& inDigest) -> void{
    this->digest = DataBlob{inDigest.c_str()};
    //assert(this->digest.size() == DIGEST_SIZE);
}

auto Digest::asBigNumber() -> BigNumber{
    //assert(this->digest.size() == DIGEST_SIZE);
    return this->digest.toBigNumber();
}
auto Digest::asStdString() -> std::string{
    //assert(this->digest.size() == DIGEST_SIZE);
    return this->digest.toStdString();
}
auto Digest::size() -> int{
    return this->digest.size();
}

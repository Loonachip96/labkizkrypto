#include "DataBlob.h"
#include <cstring>
#include <functional>

DataBlob::~DataBlob(){
}

DataBlob::DataBlob()
    : DataBlob("")
{
}

DataBlob::DataBlob(const DataBlob& inObj){
    this->allocate(inObj.blobSize);
    
    memcpy(
        static_cast<void*>( blobBytes.get() ),
        static_cast<void*>( (char*) inObj.blobBytes.get() ),
        blobSize
    );

    this->blobSize = inObj.blobSize;
}

DataBlob::DataBlob(DataBlob&& inObj){
    this->allocate(inObj.blobSize);
    this->blobBytes = std::move(inObj.blobBytes);
    this->blobSize = std::move(inObj.blobSize);
}

DataBlob::DataBlob(CryptoPP::byte* bytes, size_t bytesAmount){
    if( 0 < bytesAmount){
        this->allocate( bytesAmount );
        memcpy(
            static_cast<void*>( blobBytes.get() ),
            static_cast<void*>( (char*) bytes ),
            blobSize
        );
    }
    this->blobSize = bytesAmount;
}

DataBlob::DataBlob(const char* data){
    size_t dataLength = strlen(data);

    if( 0 < dataLength){
        this->allocate( dataLength );
        memcpy(
            static_cast<void*>( blobBytes.get() ),
            static_cast<void*>( (char*) data ),
            blobSize
        );
    }
    this->blobSize = dataLength;
}

void DataBlob::allocate(size_t size){
    if(this->blobBytes)
        this->blobBytes.~shared_ptr();

    if(0 < size)
        blobBytes = std::shared_ptr<Blob_t>( new CryptoPP::byte [size] );

    blobSize = size;
}

CryptoPP::byte& DataBlob::operator[](int i){
    return this->blobBytes[i];
}

auto DataBlob::operator=(DataBlob&& newBlob) -> DataBlob&{
    if(this->blobBytes)
        this->blobBytes.~shared_ptr();

    if(newBlob.blobBytes)
        this->blobBytes = std::move(newBlob.blobBytes);

    this->blobSize = newBlob.blobSize;

    return *this;
}

auto DataBlob::size() -> size_t{
    return this->blobSize;
}

auto DataBlob::toCryptoBytesPtr() -> CryptoPP::byte*{
    return this->blobBytes.get();
}

auto DataBlob::toBigNumber() -> BigNumber{
    return BigNumber{ this->toStdString() };
}

auto DataBlob::toStdString() -> std::string{
    return std::string{ reinterpret_cast<const char*>(this->blobBytes.get()) };
}
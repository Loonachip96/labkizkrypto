#pragma once

#include "Node.h"
#include "TypeDefs.h"
#include <TBB/flow_graph.h>

class CargoConveyorFactory : public NodeFactory< tbb::flow::queue_node<TaggedDebris>, NodeInfoTag_t >{
public:
    CargoConveyorFactory(tbb::flow::graph* graphPtr) : NodeFactory(graphPtr) {}
    NodeInstance_t Create(int objectID) override;
};

template<int TAmount>
class CargoConveyorNodes : public NodeArray< TAmount, CargoConveyorFactory::Node_t, CargoConveyorFactory::NodeData_t >
{
public:
    CargoConveyorNodes(tbb::flow::graph& g)
        : NodeArray< TAmount, CargoConveyorFactory::Node_t, CargoConveyorFactory::NodeData_t >( 
            std::make_shared<CargoConveyorFactory>(&g) 
        )
    { }
    ~CargoConveyorNodes() = default;
};
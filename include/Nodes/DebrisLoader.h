#pragma once

#include "Node.h"
#include "TypeDefs.h"
#include <TBB/flow_graph.h>

class DebrisLoaderFunctional{
private:
    const int ID;
    TaggedDebrisCart cargoCart;
public:
    DebrisLoaderFunctional(int objectID) : ID{objectID} { }
    auto operator()(TaggedDebris taggedDebris) -> TaggedDebrisCart;
};

class DebrisLoaderFactory : public NodeFactory< tbb::flow::function_node<TaggedDebris, TaggedDebrisCart>, NodeInfoTag_t >{
public:
    DebrisLoaderFactory(tbb::flow::graph* graphPtr) : NodeFactory(graphPtr) {}
    NodeInstance_t Create(int objectID) override;
};

template<int TAmount>
class DebrisLoaderNodes : public NodeArray< TAmount, DebrisLoaderFactory::Node_t, DebrisLoaderFactory::NodeData_t >
{
public:
    DebrisLoaderNodes(tbb::flow::graph& g)
        : NodeArray< TAmount, DebrisLoaderFactory::Node_t, DebrisLoaderFactory::NodeData_t >( 
            std::make_shared<DebrisLoaderFactory>(&g) 
        )
    { }
    ~DebrisLoaderNodes() = default;
};
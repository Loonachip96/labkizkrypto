#pragma once
#include <chrono>
#include <thread>
#include <functional>

class PeriodWaiterTimer{
public:
    PeriodWaiterTimer(std::chrono::milliseconds period){
        std::this_thread::sleep_for(period);
    }
};

template<typename TExpectedValue>
class BusyPollTimer{
public:
    BusyPollTimer(
        std::chrono::milliseconds pollInterval, 
        TExpectedValue expectedValue, 
        std::function<TExpectedValue()> callback
    )
    {
        TExpectedValue actualValue;
        while(true){
            actualValue = callback();
            if(actualValue == expectedValue)
                break;
            else
                PeriodWaiterTimer{pollInterval};
        }
    }
};

class Timer{
private:
    std::chrono::high_resolution_clock::time_point startPoint;
public:
    Timer(){
        startPoint = std::chrono::high_resolution_clock::now();
    }
    template<typename TTimeResolution>
    TTimeResolution timeElapsed(){
        return std::chrono::duration_cast<TTimeResolution>(std::chrono::high_resolution_clock::now() - startPoint).count();
    }
};
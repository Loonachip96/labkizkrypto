#pragma once

#include "DataBlob.h"
#include <CryptoCpp/sha.h>
#include <BigNumber/bignumber.h>

class Digest{
public:
    constexpr static const size_t DIGEST_SIZE = CryptoPP::SHA256::DIGESTSIZE;
    
    ~Digest();
    Digest();
    Digest(std::string inDigest);
    Digest(const Digest& inDigest);
    Digest(Digest&& inDigest);

    auto operator=(BigNumber inDigest) -> Digest&;
    auto operator[](int i) -> int;

    auto calculateDigestFor(std::string& message) -> void;
    auto assignDigest(CryptoPP::byte* inDigest) -> void;
    auto assignDigest(BigNumber inDigest) -> void;
    auto assignDigest(std::string& inDigest) -> void;

    auto asBigNumber() -> BigNumber;
    auto asStdString() -> std::string;
    auto size() -> int;

private:
    DataBlob digest;
};
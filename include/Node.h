#pragma once

#include <TBB/flow_graph.h>
#include <array>
#include <functional>
#include <memory>


template<class TNode, typename TNodeData>
class NodeFactory{
protected:
    tbb::flow::graph* _graphPtr;

public:
    using Node_t = TNode;
    using NodeData_t = TNodeData;
    using NodeInstance_t = struct _NodeInstance{
        std::shared_ptr<Node_t> node;
        std::shared_ptr<NodeData_t> data;
    };

    NodeFactory(tbb::flow::graph* graphPtr) 
        : _graphPtr(graphPtr) 
    { }

    virtual NodeInstance_t Create(int objectID) = 0;
};


template<int TNodesAmount, class TNode, class TNodeData>
class NodeArray{
public:
    constexpr static const int capacity{TNodesAmount};

public:
    using Factory_t = NodeFactory<TNode, TNodeData>;
    using Node_t = typename Factory_t::Node_t;
    using NodeData_t = typename Factory_t::NodeData_t;
    using NodeInstance_t = typename Factory_t::NodeInstance_t;

protected:
    std::array<std::shared_ptr<NodeInstance_t>, TNodesAmount> nodeArray;

public:
    NodeArray( std::shared_ptr<Factory_t> nodeFactory ) {
        this->initialise(nodeFactory);
    }

    auto getNode() -> Node_t&{
        return *(this->nodeArray.at(0))->node;
    }

    auto getNode(int i) -> Node_t&{
        return *(this->nodeArray.at(i))->node;
    }

private:
    auto initialise(
        std::shared_ptr<Factory_t> nodeFactory
    ) -> void {
        for (size_t i = 0; i < this->nodeArray.size(); i++){
            this->nodeArray.at(i) = std::make_shared<NodeInstance_t>( std::move( nodeFactory->Create(i) ) );
        }
    }

};

#pragma once

#include "DataBlob.h"
#include <BigNumber/bignumber.h>
#include <ctime>

class BlockHeader{
public:
    BlockHeader();
    ~BlockHeader();

    BigNumber rootMerkleDigest{0};
    BigNumber prevBlockDigest{0};
    time_t timestamp{0};
    BigNumber prevBlockID{0};
    BigNumber randomNumber{0};
    

    auto getDigest() -> BigNumber;

private: 
    auto serialise()-> DataBlob;
};
